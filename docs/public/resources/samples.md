# Sample Add-ons

The following repositories contain sample Atlassian Connect add-ons. This list is growing all the time, so check back often.

## Atlassian Connect Express (node.js) add-ons
- [Sequence Diagramr](https://bitbucket.org/atlassianlabs/atlassian-connect-confluence-sequence-diagramr) – A Confluence macro for creating UML sequence diagrams on the page.
- [Tim's Word Cloud](https://bitbucket.org/atlassianlabs/atlassian-connect-confluence-word-cloud) – A macro that takes the contents of a page and constructs an SVG based word cloud.
- [Webhook Inspector](https://bitbucket.org/atlassianlabs/webhook-inspector) – Inspects the response bodies of the available webhooks in Atlassian Connect.

## Play Framework (Java) add-ons
- [Who's Looking](https://bitbucket.org/atlassian/whoslooking-connect/overview) – Displays the users who are currently looking at a JIRA issue.  
- [JIRA Autowatcher](https://bitbucket.org/atlassianlabs/atlassian-autowatch-plugin) - automatically add watchers to newly created JIRAs
 
## Other
- [Google Maps macro](https://bitbucket.org/acgmaps/acgmaps.bitbucket.org) - insert Google Maps into your Confluence pages
- [Who's Looking Front-End Only Prototype](https://bitbucket.org/atlassianlabs/atlassian-connect-whoslooking-connect-v2) – similar functionality to the original [Who's Looking](https://bitbucket.org/atlassian/whoslooking-connect/overview) Play-based add-on, but written entirely using static HTML, JS & CSS using [Firebase](https://www.firebase.com/).

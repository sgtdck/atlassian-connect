<h1 class="index-heading">Introducing Atlassian Connect</h1>

<h2 class="index-heading">Build, install and sell add-ons for JIRA and Confluence OnDemand</h1>

<div class="index-video-container">
    <a href="//fast.wistia.net/embed/iframe/3e1auia2xi?popover=true" class="wistia-popover[height=540,playerColor=205081,width=960,helpers.overlay.css.backgroundColor=#000,helpers.overlay.opacity=1,padding=20]">
        <div class="inner video-thumbnail">
            <div class="playButton"></div>
        </div>
    </a>
<script charset="ISO-8859-1" src="//fast.wistia.com/assets/external/popover-v1.js"></script>
</div>


## What is Atlassian Connect?
You can use the Atlassian Connect framework to build add-ons for Atlassian applications
 like JIRA, Confluence, and HipChat. An add-on could be an integration with another existing service, 
 new features for the Atlassian application, or even a new product that runs within the Atlassian application.

## What is an Atlassian Connect add-on?
Simply understood, Atlassian Connect add-ons are web applications.
Atlassian Connect add-ons operate remotely over HTTP and can be written with any programming
language and web framework.

Fundamentally, Atlassian Connect add-ons have three major capabilities:

1. Insert content in [certain defined places](./modules/jira/index.html) in the Atlassian application's UI.
2. Make calls to the Atlassian application's [REST API](./rest-apis/product-api-browser.html).
3. Listen and respond to [WebHooks](./modules/jira/webhooks.html) fired by the Atlassian application.

<div class="closing-cta">
    <a href="./guides/getting-started.html">
        <button class="primary-cta aui-button aui-button-primary">
            Try the Hello World example
        </button>
    </a>
    <p><a href="./guides/introduction.html">or read the detailed introduction</a></p>
</div>





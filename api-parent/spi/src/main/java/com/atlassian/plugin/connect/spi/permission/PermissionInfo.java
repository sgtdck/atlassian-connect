package com.atlassian.plugin.connect.spi.permission;

/**
 * @since 0.8
 */
public interface PermissionInfo
{
    String getName();

    String getDescription();
}

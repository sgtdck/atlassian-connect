package com.atlassian.plugin.connect.spi.applinks;

import com.atlassian.applinks.spi.application.NonAppLinksApplicationType;

/**
 * Application type for all remote plugin container links
 */
public interface RemotePluginContainerApplicationType extends NonAppLinksApplicationType
{
}

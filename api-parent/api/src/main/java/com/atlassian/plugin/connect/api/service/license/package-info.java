/**
 * This is a temporary service for license retrieval until the UPM can provide a proper asynchronous service
 */
package com.atlassian.plugin.connect.api.service.license;
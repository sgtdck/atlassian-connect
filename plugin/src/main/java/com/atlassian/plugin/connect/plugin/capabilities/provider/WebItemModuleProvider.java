package com.atlassian.plugin.connect.plugin.capabilities.provider;

import com.atlassian.plugin.connect.modules.beans.WebItemModuleBean;

public interface WebItemModuleProvider extends ConnectModuleProvider<WebItemModuleBean>
{

}

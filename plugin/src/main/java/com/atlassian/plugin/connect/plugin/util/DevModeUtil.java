package com.atlassian.plugin.connect.plugin.util;

import com.atlassian.plugin.util.PluginUtils;

public class DevModeUtil
{

    public static boolean DEV_MODE_ENABLED = Boolean.getBoolean(PluginUtils.ATLASSIAN_DEV_MODE);

}

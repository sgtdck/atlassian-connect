package com.atlassian.plugin.connect.plugin.iframe.render.strategy;

/**
 *
 */
public interface IFrameRenderStrategyBuilderFactory
{
    IFrameRenderStrategyBuilder builder();
}

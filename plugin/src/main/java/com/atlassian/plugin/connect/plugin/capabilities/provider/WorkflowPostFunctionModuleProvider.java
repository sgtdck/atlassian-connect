package com.atlassian.plugin.connect.plugin.capabilities.provider;

import com.atlassian.plugin.connect.modules.beans.WorkflowPostFunctionModuleBean;

public interface WorkflowPostFunctionModuleProvider extends ConnectModuleProvider<WorkflowPostFunctionModuleBean>
{

}

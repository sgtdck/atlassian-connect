package com.atlassian.plugin.connect.plugin.service;

public interface IsDevModeService
{
    public boolean isDevMode();
}
